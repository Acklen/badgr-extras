const zipFolder = require("zip-folder");
const SRC_FOLDER_NAME = "extension";
const ZIP_NAME = "badgr_extras.zip";

// Pack extension in a zip file.
function pack() {
  zipFolder(SRC_FOLDER_NAME, ZIP_NAME, (error) => {
    if (error) {
      console.error("Error generating .zip file: ", error);
    } else {
      console.info(
        `Successfully zipped the "${SRC_FOLDER_NAME}" directory and store as "${ZIP_NAME}".`
      );
    }
  });
}

pack();

module.exports = { pack };

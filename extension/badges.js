Date.prototype.addDays = function (days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};

function checkPathways() {
  var tags = $("body").find("div.tag").toArray();
  for (let index = 0; index < tags.length; index++) {
    var parts = String(tags[index].innerHTML).split(":");
    if (parts[0] === "pathway") {
      return parts[1];
    }
  }
  return "";
}

function getBadgeId() {
  var badgrURL = window.location.href;
  var parts = String(badgrURL).split("/");
  const findLast = (elem, index) => index === parts.length - 1;
  return parts.find(findLast);
}

function validateJSON(json) {
  var checkedjson;
  try {
    checkedjson = JSON.parse(json);
  } catch (e) {
    checkedjson = JSON.parse(`{"email" : "${json}", "bagde_ids" : []}`);
    createBadgeCookie("email", json);
  }
  return checkedjson;
}

// TODO: reimplement this function.
function createBadgeCookie(name, value) {
  var today = new Date();
  var obj = `{"email" : "${value}", "bagde_ids" : []}`;
  document.cookie =
    name + "=" + escape(obj) + "; expires=" + today.addDays(30).toGMTString();
}

// TODO: reimplement this function.
function saveBadgeCookie(name, user_email, id) {
  var obj = JSON.parse(getCookie(name));
  const found = obj.bagde_ids.find((bid) => bid === id);

  if (!found && obj.email === user_email) {
    obj.bagde_ids.push(id);
    document.cookie = name + "=" + escape(JSON.stringify(obj));
    $("#claim_btn").addClass("disabled");
  }
}

// TODO: reimplement this function.
function getCookie(name) {
  var re = new RegExp(name + "=([^;]+)");
  var value = re.exec(document.cookie);
  return value != null ? unescape(value[1]) : null;
}

function goToClaim() {
  closeModal();
  $("#claim_btn").addClass("disabled");
  var learnerEmail = document.querySelector("input#email").value;
  var badgeId = getBadgeId();
  const headingElem = document.querySelector("bg-heading#bg-heading-2");
  var badgeName = headingElem
    ? headingElem.textContent.trim()
    : "no-badge-name";
  var evidence = document.querySelector("textarea#evidence").value;

  if (getCookie("email") == null) {
    createBadgeCookie("email", learnerEmail);
  }

  const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (emailRegex.test(learnerEmail)) {
    fetch(`https://eo1rquyg5xv4cfl.m.pipedream.net`, {
      method: "POST",
      body: JSON.stringify({
        email: learnerEmail,
        badgeId: badgeId,
        badgeName: badgeName,
        evidence: evidence,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        // it should have: response.status === 200
        // console.log("RESPONSE", res)
        $("#snackbar").addClass("show");
        saveBadgeCookie("email", learnerEmail, badgeId);
        setTimeout(function () {
          $("#snackbar").removeClass("show");
        }, 3000);
        $("#go_claim").attr("disabled", true);
      })
      .catch((error) => {
        console.error(error);
        $("#snackbar_error").html("Something bad happen with the request");
        $("#snackbar_error").addClass("show");
        setTimeout(function () {
          $("#snackbar_error").removeClass("show");
        }, 3000);
      });
  } else {
    $("#snackbar_error").html("Invalid Email");
    $("#snackbar_error").addClass("show");
    setTimeout(function () {
      $("#snackbar_error").removeClass("show");
    }, 3000);
  }
}

function openModal() {
  var obj = JSON.parse(getCookie("email"));
  if (obj != null) {
    $("#email").val(obj.email);
  }
  $("body").find("#claim_modal").css("display", "inline");
}

function closeModal() {
  $("body").find("#claim_modal").css("display", "none");
}

function addButton(pathway) {
  var obj = validateJSON(getCookie("email"));
  var disabled = "";
  var innerText = pathway === "" ? "Claim Badge" : "View Pathway";
  if (obj) {
    const found = obj.bagde_ids.find((bid) => bid === getBadgeId());
    disabled = pathway === "" && found ? "disabled" : "";
  }
  var $input = $(
    '<button type="button" id="claim_btn" class="button button-primary ' +
      disabled +
      '">' +
      innerText +
      "</button>"
  ).click(
    pathway === ""
      ? openModal
      : function () {
          window.location = "https://badgr.com/public/pathway/" + pathway;
        }
  );
  $("body").find("div.l-stack").last().find("button").before($input);
}

function addBadgeModal() {
  let tooltip = `You can paste any evidence here that proves you have earned this badge or mastered the competencies/criteria.
  If you want to send a photo, please paste a link.`;
  let $modal = $(
    '<div id="snackbar">The request was sent successfully</div>' +
      '<dialog-layout role="dialog" id="claim_modal" style="display: none;" dialogtitle="Claim Badge" class="dialog l-dialog dialog-is-active ng-star-inserted" aria-hidden="false">' +
      '<div id="snackbar_error"></div>' +
      '<div class="dialog-x-box o-container o-container-is-managed o-container-is-atleast480px o-container-is-atmost768px o-container-is-atmost1024px o-container-is-atmost1440px" style="">' +
      "<bg-dialog-messages></bg-dialog-messages>" +
      '<div class="l-flex l-flex-justifybetween u-padding-top2x u-padding-xaxis3x">' +
      '<h2 class="u-text u-text-h2 u-text-dark3 ng-star-inserted" id="null">Claiming this badge</h2>' +
      '<div class="l-flex l-flex-0p5x">' +
      '<button id="closeModal" class="buttonicon ng-star-inserted" style="font-size: 16px;">X' +
      "</button>" +
      "</div>" +
      "</div>" +
      '<div class="u-padding-all3x">' +
      '<bg-recipient-verification-panel requestidentifiertitle="" requestidentifiermessage="Enter your email address to claim this badge.">' +
      '<p class="ng-star-inserted"> Enter your email address to claim this badge. </p>' +
      "<div>" +
      '<bg-formfield-text label="Email" class="forminput forminput-required ng-star-inserted">' +
      '<div class="forminput-x-labelrow">' +
      '<label class="forminput-x-label" for="email"> Email </label>' +
      "</div>" +
      '<div class="forminput-x-inputs">' +
      '<input type="email" name="email" id="email" placeholder="" class="ng-pristine ng-invalid ng-star-inserted ng-touched">' +
      "</div>" +
      "</bg-formfield-text>" +
      '<bg-formfield-text label="Evidence" class="forminput ng-star-inserted">' +
      "<div>" +
      '<div id="element"> <label class="forminput-x-label"> Evidence </label> </div>' +
      `<div id="info-icon" tooltip="${tooltip}" tooltip-position="right">` +
      '<svg id="info-svg" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm1 18h-2v-8h2v8zm-1-12.25c.69 0 1.25.56 1.25 1.25s-.56 1.25-1.25 1.25-1.25-.56-1.25-1.25.56-1.25 1.25-1.25z"/></svg>' +
      "</div>" +
      "</div>" +
      '<div class="forminput-x-inputs">' +
      '<textarea name="email" rows="4" cols="50" id="evidence" maxlength="300" class="ng-pristine ng-invalid ng-star-inserted ng-touched">' +
      "</textarea>" +
      "</div>" +
      "</bg-formfield-text>" +
      '<div class="l-stack l-stack-buttons l-stack-2x u-margin-top2x">' +
      '<button id="go_claim" class="button">Continue</button>' +
      '<button id="close_modal_btn" class="button button-secondary ng-star-inserted">Cancel</button>' +
      "</div>" +
      "</div>" +
      "</bg-recipient-verification-panel>" +
      "</div>" +
      "</div>" +
      "</dialog-layout>"
  );
  $("body").find("div.l-stack").last().before($modal);
  $("body").find("#closeModal").click(closeModal);
  $("body").find("#close_modal_btn").click(closeModal);
  $("body").find("#go_claim").click(goToClaim);
}

function cleanUrl(incoming_url) {
  const regex = new RegExp("/[^/]*$");
  if (incoming_url.replace(regex, "/") === "https://badgr.com/public/badges/") {
    const processed = incoming_url.replace(regex, "/").split("//")[1];
    return processed;
  } else {
    const index = incoming_url.indexOf(".");
    const splits = [
      incoming_url.slice(0, index),
      incoming_url.slice(index + 1),
    ];
    const processed = splits[1].replace(regex, "/");
    return processed;
  }
}

function renderClaimBadgeButton() {
  var base_url = "badgr.com/public/badges/";
  var incoming_url = window.location.href;
  const result = cleanUrl(incoming_url);
  if (result === base_url) {
    setTimeout(function () {
      if ($("#claim_btn").length == 0) {
        let pathway_id = checkPathways();
        let awarded_expected_message = "This badge has been awarded to you";
        let badgeAlreadyClaimedInfo = document.querySelector(
          "bg-page-top-info-bar"
        );
        let received_message = "";
        if (badgeAlreadyClaimedInfo) {
          received_message =
            badgeAlreadyClaimedInfo.getAttribute("messagetext");
        }
        // TODO: complete `setCriteria` function.
        // setCriteria();
        addButton(pathway_id);
        const isAlreadyAwarded = received_message.includes(
          awarded_expected_message
        )
          ? true
          : false;
        if (!isAlreadyAwarded) {
          if (pathway_id === "") {
            addBadgeModal();
          }
        } else {
          $("#claim_btn").addClass("disabled");
        }
      }
    }, 2000);
  }
}

// Deprecated: using content_scripts at manifest.json
// Verify response to content background service worker
// chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
//   console.log(request, sender, sendResponse);
//   renderClaimBadgeButton(request.message, sender);
// });

renderClaimBadgeButton();
